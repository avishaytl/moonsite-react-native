import { Dimensions } from 'react-native';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import { Platform } from 'react-native';

const statusbar = getStatusBarHeight();
const width = Dimensions.get('window').width;
const height = Platform.OS !== 'ios' ? Dimensions.get('window').height + statusbar : Dimensions.get('window').height;
console.debug('is iphone - ' + Platform.OS);
console.log('status bar - ' + statusbar);
console.log('is Height - ' + height);
console.debug('is width - ' + width);

const primaryTheme = {
    $deviceWidth: width,
    $deviceHeight: height,
    $deviceStatusBar: statusbar,  
    activeTheme:{ 
        $title:'#000',
        $text: '#fff',
        $background:'#fff',
        $icons:'#fff',
        $themeMenuBackground: 'rgba(255,255,255,0.3)',
    },
    darkTheme:{
        $title:'#fff', 
        $background:'#000',
        $icons:'#fff',
        $text: '#fff',
        $themeMenuBackground: 'rgba(255,255,255,0.3)',
    },
    lightTheme:{
        $title:'#000', 
        $background:'#fff',
        $icons:'#000',
        $text: '#fff',
        $themeMenuBackground: 'rgba(0,0,0,0.3)',
    }
};

export default primaryTheme;
