import Screens from './screens/Screens'
import React from 'react'
import { Provider } from 'react-redux'
import configureStore from './store/store'
import { rootSaga } from './sagas/index'
export const store = configureStore();
store.runSaga(rootSaga);
export default class App extends React.Component{ 
    render(){
        return(
            <Provider store={store}>
                <Screens/>
            </Provider>
        );
    }
}
