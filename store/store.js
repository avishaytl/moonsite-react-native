
import rootReducer from '../reducers/index'
import { createStore, applyMiddleware, compose } from 'redux'
import { createLogger } from 'redux-logger'
import createSagaMiddleware from 'redux-saga'

export const sagaMiddleware = createSagaMiddleware();
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const middlewares = [sagaMiddleware]; // thunk,
if (process.env.NODE_ENV === 'development') {
    const log = createLogger({
        predicate: (getState, action) => action.type !== 'Navigation/COMPLETE_TRANSITION',
        collapsed: (getState, action, logEntry) => !logEntry.error,
    });
    middlewares.push(log);
}

export default function configureStore(initialState) {
    return{ ...createStore(rootReducer, initialState,  composeEnhancers(applyMiddleware(...middlewares))), runSaga: sagaMiddleware.run};
}