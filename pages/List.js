import React from 'react'
import {  View, StyleSheet, Text, ScrollView} from 'react-native'
import { connect } from 'react-redux'
import ButtonApp from '../components/ButtonApp'
import { Icon } from 'react-native-elements'
import { TouchableOpacity } from 'react-native-gesture-handler'
import primaryTheme from '../styles/styles'
import ModalApp from '../components/ModalApp'
import primaryValues from '../constants/values'
class List extends React.Component{
  constructor(props){
    super(props);
    this.state = {  
      mainTitle: 'Movies List',
      btnName:'Sign Out',
      modalActive: false,
      movies_list: this.props.initialState.movies_list,
      itemSelected: null,
    } 
  }   
  btnClick = () =>{
    this.props.navigation.navigate('LoginScreen');
  }
  movieClick = (movie) =>{
    console.log(movie);
    this.setState({itemSelected: movie});
    this.setState({modalActive: true});
  }
  getMovies = () =>{
    let data = [];
    let movies = this.props.initialState.movies_list;
    for(let i = 0;i < movies.length;i++)
      data.push(
        <TouchableOpacity onPress={()=> this.movieClick(movies[i])} key={movies[i].key} style={[styles.itemList,{backgroundColor: primaryTheme.activeTheme.$themeMenuBackground}]}>
          <Text style={{color: '#fff',fontSize:16,textAlign:'center',fontWeight:'bold'}}>{movies[i].vote}</Text>  
            <Icon name='star' type='font-awesome' color={'#ffeb3b'} style={{margin:5}}/>
          <Text style={{color: '#fff',fontSize:16,textAlign:'center'}}>{movies[i].title}</Text> 
        </TouchableOpacity>
      );
    return data;
  }
  closeModal = () =>{
    this.setState({modalActive: false});
  }
  deleteMovie = () =>{ 
    this.props.dispatch({type: primaryValues.$DELETE_MOVIE, item: this.state.itemSelected, movies: this.props.initialState.movies_list}); 
    this.setState({modalActive: false});
  }
    render(){  
      return(
          <View style={[styles.container,{backgroundColor: primaryTheme.activeTheme.$background}]}> 
            <View style={styles.main}>
                {this.state.modalActive ? <ModalApp onPress={this.deleteMovie} item={this.state.itemSelected} closeModal={this.closeModal}/> : null}
                <ScrollView style={{width:'100%',height:'100%'}}>
                  <View style={{justifyContent:'center',alignItems:'center'}}>
                    <Text style={[styles.mainTitle,{color: primaryTheme.activeTheme.$title}]}>{this.state.mainTitle}</Text>
                    {this.props.initialState.movies_list.length === 0 ? <Text style={{color:  primaryTheme.activeTheme.$title,fontSize:16,textAlign:'center',fontWeight:'bold'}}>Empty List</Text> : this.getMovies()}
                    <ButtonApp name={this.state.btnName} colors={'#db4c3f'} onPress={this.btnClick} icon={<View style={{marginLeft:5}}><Icon name='logout' type='antdesign' color={'#fff'} size={30}/></View>}/> 
                  </View>
                </ScrollView>
            </View>
          </View>
      );
    } 
}

  

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor:'#000', 
      alignItems: 'center',
      justifyContent:'center',
    }, 
    main:{
      height: '100%',
      width: primaryTheme.$deviceWidth,
      position: 'absolute', 
      alignItems: 'center',
      justifyContent:'center',
    }, 
    icon:{
      fontSize:20,
      marginLeft:10,
      color:'#fff'
    },  
    mainTitle:{
      fontSize:30,
      color: '#fff',
      fontWeight:'bold', 
    },
    itemList:{
      height:30,
      width:200,
      backgroundColor:'rgba(255,255,255,0.3)',
      flexDirection:'row',
      alignItems: 'center',
      justifyContent:'center',
      margin:5
    }
  });
    

  const mapStateToProps = (state) => {
    return {
      initialState: state.initialState,
    };
  };
  // Exports
  export default connect(mapStateToProps)(List);