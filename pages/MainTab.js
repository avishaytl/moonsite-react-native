import React from 'react'
import { Image, View, StyleSheet, ScrollView, Text} from 'react-native'
import { connect } from 'react-redux'
import primaryTheme from '../styles/styles'
import { Icon } from 'react-native-elements'
import ButtonApp from '../components/ButtonApp'
import primaryValues from '../constants/values'
class MainTab extends React.Component{
  constructor(props){
    super(props);
    this.state = {  
      item: this.props.navigation.state.params.item,
      btnName: 'Add ' + this.props.navigation.state.params.item.title + ' to List',
    }  
  } 
  saveMovie = () =>{ 
    this.props.dispatch({type: primaryValues.$SAVE_MOVIE, item: this.state.item, movies: this.props.initialState.movies_list}); 
  }
    render(){ 
      return(
          <View style={[styles.container,{backgroundColor: primaryTheme.activeTheme.$background}]}>  
              <View style={styles.main}>
                  <ScrollView style={{width:'100%',height:'100%'}}>
                    <View style={styles.imgView}>
                      <Image source={{uri:'https://image.tmdb.org/t/p/w500' + this.state.item.imgUrl}} style={{width:'100%',height:'100%',resizeMode:'stretch'}}/>  
                    </View> 
                    <Text style={[styles.title,{color: primaryTheme.activeTheme.$title}]}>{this.state.item.title}</Text>
                    <View style={styles.voteView}>
                        <View style={{position:'absolute'}}>
                            <Icon name='star' type='font-awesome' color={'#ffeb3b'} size={70}/> 
                        </View>
                        <Text style={styles.voteText}>{this.state.item.vote}</Text>
                    </View> 
                    <Text style={[styles.info,{color: primaryTheme.activeTheme.$text,backgroundColor: primaryTheme.activeTheme.$themeMenuBackground}]}>{this.state.item.overview}</Text>
                    <View style={{width: primaryTheme.$deviceWidth,justifyContent:'center',alignItems:'center'}}>
                      <ButtonApp onPress={this.saveMovie} colors={'#3d8b40'} name={this.state.btnName} icon={<View style={{marginLeft:5}}><Icon name='add-to-list' type='entypo' color={'#fff'} size={30}/></View>}/> 
                    </View>
                  </ScrollView>
              </View>  
          </View>
      );
    }
  
  
}

  

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor:'black', 
      alignItems: 'center',
      justifyContent:'center',
    },
    main:{
        height: '100%',
      width: primaryTheme.$deviceWidth, 
      position: 'absolute', 
      alignItems: 'center',
      justifyContent:'flex-start',
    }, 
    imgView:{
      width: primaryTheme.$deviceWidth,
      height:500, 
    },
    title:{
      fontSize:40,
      fontWeight:'bold',
      textAlign:'center',
      color: '#fff'
    },
    info:{
      fontSize:16, 
      textAlign:'center',
      color: '#fff',
      margin:20,
      padding:10
    }, 
    voteText:{
      fontSize:20, 
      fontWeight:'bold',
      textAlign:'center',
      color: '#000'
    },
    voteView:{
      height:80,
      justifyContent:'center',
      alignItems:'center',
      flexDirection:'row'
    },
    btnIcon:{
      fontSize:20,
      marginLeft:10,
      color: '#fff'
    }
  });
    

  const mapStateToProps = (state) => {
    return {
      initialState: state.initialState,
    };
  };
  // Exports
  export default connect(mapStateToProps)(MainTab);