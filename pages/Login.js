import React from 'react'
import { Animated, View, StyleSheet, Text, Image, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'
import primaryTheme from '../styles/styles'
import ButtonApp from '../components/ButtonApp'
import { Icon } from 'react-native-elements'
import primaryValues from '../constants/values' 
import { LoginManager, AccessToken } from "react-native-fbsdk"
import { GoogleSignin, statusCodes } from 'react-native-google-signin'
class Login extends React.Component{
  constructor(props){
    super(props);
    this.state = {   
      signinAnime: new Animated.Value(0), 
      themeAnime: new Animated.Value(0),  
      signedIn: false,
      mainTitle: 'Welcome',
      mainText: 'Please log in to continue to the awesomness',
      userName: 'Stranger',
      imgUrl: null,
      btnLeftText: 'Login with Facebook',
      btnRightText: 'Or with Google', 
      accessToken: null, 
    } 
  }    
  async componentDidMount(){
  }
 facebookSignin = async () =>{
    let token = this.state.accessToken;
    const response = await fetch(`https://graph.facebook.com/me?access_token=${token}`); 
    let info = (await response.json()); 
    console.log("info",info); 
    const url = await fetch(`https://graph.facebook.com/${info.id}/picture?redirect=0&height=200&width=200&type=normal`);
    let urlimg = (await url.json()); 
    console.log("info",urlimg.data.url); 
    this.setState({
        signedIn: true,
        userName: info.name,
        imgUrl: urlimg.data.url
    });
    this.signinAnime();
 } 
  sigininGoogle = async () => { 
    if(this.state.themeAnime._value === 0)   
        try {
            await GoogleSignin.configure({ 
                scopes: ["profile", "email"],
                androidClientId: "686931655678-dndvigraiflgphsruggjthg5u5jopai1.apps.googleusercontent.com", 
            }); 
            await GoogleSignin.hasPlayServices();
            const userInfo = await GoogleSignin.signIn();
            // this.setState({ userInfo: userInfo }); 
            console.log('userInfo',userInfo);
            this.setState({
                signedIn: true,
                userName: userInfo.user.name,
                imgUrl: userInfo.user.photo
            });
            this.signinAnime();
        } catch (error) { 
            alert(`Google login Error: ${message}`); 
        }
    else
        this.themeAnime(); 
  }
  sigininFacebook = async () => { 
    if(this.state.themeAnime._value === 0)   
      try {
        LoginManager.logInWithPermissions(["public_profile"]).then(
            (result) => {
            if (result.isCancelled) {
                alert("Login cancelled");
            } else {
                console.log('result',result.grantedPermissions);
                    AccessToken.getCurrentAccessToken().then( accessToken => { 
                        this.setState({accessToken: accessToken.accessToken}); 
                        this.facebookSignin();
                    });  
            }
            },
            function(error) {
                alert("Login fail with error: " + error);
            }
        );
      } catch ({ message }) {
        alert(`Facebook login Error: ${message}`);
      }
    else
      this.themeAnime();
  }
  signinAnime = () =>{
    Animated.timing(
      this.state.signinAnime,
      {toValue: 100, duration: 500}
    ).start();
  }
  themeAnime = () =>{
    Animated.timing(
      this.state.themeAnime,
      {toValue: this.state.themeAnime._value === 0 ? 100 : 0, duration: 500}
    ).start();
  }
  signinBtn = () =>{
    if(this.state.themeAnime._value === 0)                 
      this.props.dispatch({type: primaryValues.$GET_MOVIES, navigate: this.props.navigation.navigate});
    else
      this.themeAnime(); 
  }
  changeTheme = (pos) =>{
    this.themeAnime();
    this.props.dispatch({type: primaryValues.$ACTIVE_THEME, pos});
  }
  themeMenu = () =>{
    return(
      <View style={{width:100,height:primaryTheme.$deviceHeight - primaryTheme.$deviceStatusBar,position:'absolute',left: primaryTheme.$deviceWidth,top:0}}>
          <TouchableOpacity onPress={()=>this.changeTheme('dark')} style={{width:'100%',height:40,backgroundColor: primaryTheme.activeTheme.$themeMenuBackground,margin:2,justifyContent:'center',alignItems:'center'}}>
            <Text style={{color:'#fff',textAlign:'center',fontSize:18}}>Dark</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={()=>this.changeTheme('light')} style={{width:'100%',height:40,backgroundColor: primaryTheme.activeTheme.$themeMenuBackground,margin:2,justifyContent:'center',alignItems:'center'}}>
            <Text style={{color:'#fff',textAlign:'center',fontSize:18}}>Light</Text>
          </TouchableOpacity>
      </View>
    );
  }
    render(){ 
      this.props.initialState.activeTheme === 'dark' ? primaryTheme.activeTheme = primaryTheme.darkTheme : primaryTheme.activeTheme = primaryTheme.lightTheme;  
      return(
          <View style={[styles.container,{backgroundColor: primaryTheme.activeTheme.$background}]}>  
            <View style={styles.themeBtn}>
              <TouchableOpacity onPress={this.themeAnime} style={{justifyContent:'center',alignItems:'center'}}>
                <Icon name='ios-color-palette' type='ionicon' color={ primaryTheme.activeTheme.$title} size={30}/>
              </TouchableOpacity>
            </View>
            <Animated.View style={[styles.main,{right: this.state.themeAnime}]}>
              {this.themeMenu()}
              <Text style={[styles.title,{color: primaryTheme.activeTheme.$title}]}>{this.state.mainTitle + ' ' + this.state.userName}</Text> 
                <View style={styles.userImg}>
                  {this.state.signedIn ? <Image resizeMode={'stretch'} style={{width:'100%',height:'100%'}} source={{uri: this.state.imgUrl }}/> : <Text style={styles.icon}>i</Text>} 
                </View>  
                <Text style={[styles.text,{color: primaryTheme.activeTheme.$icons}]}>{this.state.signedIn ? 'Nice to meet you! \nEnjoy ツ' :this.state.mainText}</Text> 
                {/* <LoginButton
          publishPermissions={["email"]}
          onLoginFinished={
            (error, result) => {
              if (error) {
                alert("Login failed with error: " + error.message);
              } else if (result.isCancelled) {
                alert("Login was cancelled");
              } else {
                alert("Login was successful with permissions: " + result.grantedPermissions + result.name)
              }
            }
          }
          onLogoutFinished={() => alert("User logged out")}/> */}
                <Animated.View style={[styles.footer,{bottom: this.state.signinAnime}]}>
                  <ButtonApp onPress={this.sigininFacebook} colors={'#2d4f93'} name={this.state.btnLeftText} icon={<View style={{marginLeft:5}}><Icon name='facebook' type='font-awesome' color={'#fff'} size={30}/></View>}/>
                  <ButtonApp onPress={this.sigininGoogle} colors={'#db4c3f'} name={this.state.btnRightText} icon={<View style={{marginLeft:5}}><Icon name='google' type='font-awesome' color={'#fff'} size={30}/></View>}/>
                  <View style={styles.sigininBtn}>
                    <ButtonApp onPress={this.signinBtn} colors={'#3d8b40'} name={'Movie Library'} icon={<View style={{marginLeft:5}}><Icon name='library-movie' type='material-community' color={'#fff'} size={30}/></View>}/>
                  </View> 
                </Animated.View>
              </Animated.View> 
          </View>
      );
    }  
}

  

  const styles = StyleSheet.create({
    container: {
      flex:1,
      backgroundColor:'#000', 
      alignItems: 'center',
      justifyContent:'center',
    },  
    main:{
      height: '100%',
      width: primaryTheme.$deviceWidth,
      top:0, 
      position: 'absolute',
      alignItems: 'center', 
      justifyContent:'center',
    }, 
    title:{
      fontSize:30,
      fontWeight:'bold',
      textAlign:'center',
      color:'#fff'
    },
    text:{
      fontSize:18, 
      textAlign:'center' ,
      maxWidth:200,
      color:'#fff'
    },
    userImg:{
      width:150,
      height:150,
      backgroundColor:'#e8e8e8',
      borderRadius: 150/2,
      margin:30,
      alignItems:'center',
      justifyContent:'center', 
      overflow:'hidden'
    },
    footer:{
      width: primaryTheme.$deviceWidth,
      height: 100, 
      position:'absolute', 
      alignItems:'center',
      justifyContent:'center',
      flexDirection:'row'
    },
    icon:{
      fontSize:20,
      marginLeft:10,
      color:'#fff'
    },
    sigininBtn:{
      width: primaryTheme.$deviceWidth,
      height: 100,
      alignItems:'center',
      justifyContent:'center',
      position:'absolute',
      top:100
    },
    themeBtn:{
      position:'absolute',
      width:50,
      height:50, 
      top: primaryTheme.$deviceStatusBar + 10,
      left: 10,
      zIndex:9999,
    }
  });
    

  const mapStateToProps = (state) => {
    return {
      initialState: state.initialState,
    };
  }; 
  export default connect(mapStateToProps)(Login);