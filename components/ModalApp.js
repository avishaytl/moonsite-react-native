import React from 'react'
import {  StyleSheet, Image, Text, TouchableOpacity, View} from 'react-native'
import primaryTheme from '../styles/styles'
import ButtonApp from './ButtonApp'
import { Icon } from 'react-native-elements'  
export default class ModalApp extends React.Component{
  constructor(props){
    super(props);
    this.state = { 
        item: this.props.item, 
        btnName: 'remove ' + this.props.item.title
    } 
  }  
    render(){  
      return(
            <View  style={styles.container} >
                <TouchableOpacity activeOpacity={0.9} style={styles.closeView} onPress={this.props.closeModal}></TouchableOpacity> 
                <View style={{width:250,height:400,backgroundColor:'rgba(255,255,255,0.9)'}}>
                    <View style={{width:'100%',height:300}}>
                      <Image source={{uri:'https://image.tmdb.org/t/p/w500' + this.state.item.imgUrl}} style={{width:'100%',height:'100%',resizeMode:'stretch'}}/>  
                    </View>
                    <Text style={styles.mainText}>{this.state.item.title}</Text>
                    <ButtonApp onPress={this.props.onPress} icon={<Icon name={'delete'} type='antdesign' color={'#fff'} size={30}/>} colors={'#db4c3f'} name={this.state.btnName}/>
                </View>
            </View> 
      );
    } 
}

  

  const styles = StyleSheet.create({
    container: {    
        width: primaryTheme.$deviceWidth,
        height: primaryTheme.$deviceHeight, 
        backgroundColor:'rgba(0,0,0,0.7)',
        zIndex:999,
        alignItems: 'center',
        position:'absolute',
        justifyContent:'center', 
    },
    closeView:{
        width: '100%',
        height:'100%', 
        position:'absolute',
    },
    mainText:{
        fontSize:20,
        fontWeight:'bold',
        textAlign:'center'
    },
    icon:{
      fontSize:30,
      marginLeft:5,
      color:'#fff'
    },
  }); 