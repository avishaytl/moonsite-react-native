import primaryValues from '../constants/values'
const initialState = { 
  signinSuccess: false,
  movies_library:[],
  movies_list:[], 
  activeTheme: 'dark',
};

const reducer = (state = initialState, action) => {
  switch(action.type){ 
    case primaryValues.$GET_MOVIES:
        return {
          ...state, 
          signinSuccess: false,
        };
    case primaryValues.$MOVIES_RESULT:
        return {
          ...state, 
          movies_library: action.response.results,
          signinSuccess: true,
        };
    case primaryValues.$MOVIES_ERROR:
        return {
          ...state, 
          signinSuccess: false,
        };
    case primaryValues.$SAVE_MOVIE_RESULT:
        return {
          ...state,  
          movies_list: action.response.movies, 
        };
    case primaryValues.$SAVE_MOVIE_ERROR:
        return {
          ...state, 
        };
    case primaryValues.$DELETE_MOVIE_RESULT:
        return {
          ...state,  
          movies_list: action.response,
        };
    case primaryValues.$DELETE_MOVIE_ERROR:
        return {
          ...state,  
        };
    case primaryValues.$SET_THEME:
      console.log('action.pos',action.pos);
        return {
          ...state,
          activeTheme: action.pos  
        };
    default:
      return state;
  }
};

export default reducer;